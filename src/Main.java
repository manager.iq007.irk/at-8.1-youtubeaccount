import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        Account account1 = new Account();
        List<Video> likedVideos = new ArrayList<>();
        account1.addVideo(new Video("Was that a golden Freddy?", "Markiplier", 24), likedVideos);
        account1.addVideo(new Video("Hocus Pocus", "Kenny Ortega", 87), likedVideos);
        account1.addVideo(new Video("Supernatural", "Dean Winchester", 14_400), likedVideos);
        account1.addVideo(new Video("Wednesday", "MGM Television", 480), likedVideos);
        account1.addVideo(new Video("The Addams Family", "Barry Sonnenfeld", 99), likedVideos);
        account1.addVideo(new Video("Five Nights at Freddy's", "Blumhouse", 120), likedVideos);
        account1.addVideo(new Video("The Addams Family", "Barry Sonnenfeld", 99), likedVideos);

        Collections.sort(likedVideos);
        account1.printAllLiked(likedVideos);

        Collections.shuffle(likedVideos);
        Video.ascCompare(likedVideos);
        System.out.println(likedVideos);

        account1.deleteVideo("Was that a golden Freddy?",likedVideos);
        System.out.println(likedVideos);

        account1.deleteDuplicates(likedVideos);
        System.out.println(likedVideos);

    }
}

