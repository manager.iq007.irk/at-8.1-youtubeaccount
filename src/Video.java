import java.util.Collections;
import java.util.List;

public class Video implements Comparable<Video> {
    String title;
    String author;
    long duration;
    List<String> comments;
    int likes;
    int dislikes;


    public Video(String title, String author, long duration) {
        //Конструктор только с обязательными полями
        this.title = title;
        this.author = author;
        this.duration = duration;
    }

    public Video(String title, String author, long duration, List<String> comments, int likes, int dislikes) {
        //Конструктор со всеми полями
        this.title = title;
        this.author = author;
        this.duration = duration;
        this.comments = comments;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return " \n Название - \"" + title + "\"" +
                ", автор - " + author +
                ", длительность - " + duration + " мин.";
    }

    @Override
    public int hashCode() {
        return (int) (this.title.length() + this.title.charAt(0) + this.author.length() + this.author.charAt(0) + this.duration);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj != null && obj.getClass() == this.getClass()) {
            Video v = (Video) obj;
            if (v.title == null || v.author == null) {
                return false;
            } else if (v.hashCode() != this.hashCode()) {
                return false;
            } else if (v.duration != this.duration) {
                return false;
            } else {
                return this.title.length() == v.title.length() && (this.title.equals(v.title) && this.author.equals(v.author));
            }
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Video o) {    // Сортировка по длительности через Comparable
        Integer d1 = (int) this.duration;
        Integer d2 = (int) o.duration;
        return d1.compareTo(d2);
    }

    public static void ascCompare(List<Video> list) {    // Сортировка по длительности через Comparator
        Collections.sort(list, (v1, v2) -> {
            int d1 = (int) v1.duration;
            int d2 = (int) v2.duration;
            return Integer.compare(d1, d2);
        });
    }
}