import java.util.Iterator;
import java.util.List;

public class Account {
    /*
    Сделала все методы статичными т.к. все методы не нужно вызывать внутри каждого созданного аккаунта
    Они могут быть универсальными для любого объекта аккаунта, и меняться будут только передаваемые в методы переменные
     */

    public String addVideo(Video video, List<Video> list) { //Добавить видео в список
        list.add(video);
        return "Добавлено видео " + video.getTitle();
    }

    public void deleteVideo(String title, List<Video> list) { //Удалить видео по названию из списка
        try {
            Iterator<Video> iterator = list.iterator();
            while (iterator.hasNext()) {
                Video next = iterator.next();
                String substring = title;
                if (next.title.contains(substring)) {
                    iterator.remove();
                    System.out.println("Видео удалено!");
                }
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Список видео не содержит названия \"" + title + "\"!");
        }
    }


    public void printAllLiked(List<Video> list) { //Вывод всех понравившихся видео через итератор
        Iterator<Video> iterator = list.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            String likedVideo = iterator.next().toString();
            System.out.println("Понравившееся видео №" + i + ": " + likedVideo);
            i++;
        }
    }

    public void deleteDuplicates(List<Video> list) { //Удаление дубликатов через итератор
        Iterator<Video> iterator = list.iterator();
        while (iterator.hasNext()) {
            Video next = iterator.next();
            if (list.indexOf(next) + 1 < list.size()) {
                if (list.get((list.indexOf(next) + 1)).equals(next)) {
                    iterator.remove();
                }
            } else break;
        }
        System.out.println("Дубликаты удалены!");
    }
}



